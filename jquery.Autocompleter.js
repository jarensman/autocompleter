/**
 * Autocompleter
 * Adds autocomplete functionality to an input by inserting a list element after the input.
 * - Parameters such as url's can be added to the input as data attributes
 * - The list is traversable using the arrow keys
 * - Selecting items is done by clicking an item or pressing enter
 * - The autocompleted input is filled by the display value
 * - The optional value input is filled by the (usually ID) value
 * - See the individual settings for more information
 *
 * <input type="text" class="autocompleter" data-autocompleter-action="/ajax/getcompanies" data-autocompleter-param="search" />
 *
 * var input = $('input.autocompleter').Autocompleter({
 *    list: ['<ol>', { 'class': 'my-autocompleter' }],
 *    ajax: {
 *    	 dataType: 'json'
 *    },
 *    valuePath: 'Company/id',
 *    displayPath: 'Company/name',
 *    onChange: function(value, display, input) {
 *       // do something
 *    }
 * });
 *
 * The input can be triggered externally:
 * input.trigger('Autocompleter.autocomplete')
 *
 */
(function($) {
	$.fn.Autocompleter = function(params) {
		var global = $.extend({
			action: document.location.pathname, // The url to send the request to (defaults to the form action or the document.location.pathname)
			method: 'GET',                      // Request method to use
			param: 'autocomplete',              // The name of the query parameter (defaults to input name or 'autocomplete')
			named: null,                        // The name of the named parameter (mynamedparam:value) to add to the query string
			token: null,                        // The name of the token to replace in the action (when not set will send using a query parameter)
			delay: 500,                         // The delay in milliseconds to trigger after a keyup event
			value: null,                        // Selector for the input in which the value will be stored
			valuePath: '/value',                // Path inside each object to the value
			displayPath: '/display',            // Path inside each object to the display value
			allowEmpty: true,                   // Allow the value input to be cleared when the display input is cleared
			selectedClass: 'selected',          // className to apply to the element that is about to be selected (keyboard navigation)
			ajax: {                             // Ajax settings, will be passed directly to jQuery.ajax method
				dataType: 'json'
			},
			parseSettings: function(settings) { // A custom parser to parse Ajax settings right before the request is made
				return settings;
			},
			onChange: function(value, display, input) { // on change event, receives the value and display value as arguments, return true to allow the dropdown to hide.
				return true;
			},
			onInputValue: function(value) { },              // called when there are no results and the user presses 'enter'
			list: ['<ul>', { 'class': 'autocomplete' }],    // jQuery constructor arguments for the list element
			item: ['<li>', { 'class': 'autocomplete-item'}] // jQuery constructor arguments for each item in the list
		}, params), delayTimer;
		
		/**
		 * Fetches settings for the given input
		 */
		function fetchSettings(input) {
			var settings = {
				value: null
			}, form, key, dataKey;
			// fetch settings from data-autcomplete-* attributes:
			for (key in global) {
				dataKey = key.toLowerCase();
				dataKey[0] = dataKey[0].toUpperCase();
				settings[key] = input.data('autocomplete-' + dataKey) || global[key];
			}
			// set the value target input:
			if (settings.value) {
				settings.value = $(settings.value);
				if (settings.value.length == 0) {
					settings.value = null;
				}
			}
			// set possibly unset but required parameters:
			if (!settings.action || !settings.method) {
				form = input.closest('form');
				if (form.length) {
					settings.action = settings.action || form.attr('action');
					settings.method = settings.method || form.attr('method');
				}
			}
			if (!settings.token && !settings.named && !settings.param) {
				settings.param = input.attr('name') || global.param;
			}
			return $.extend(global, settings);
		}

		/**
		 * Returns the value of a child property of an object using an xpath like syntax
		 * @param path, the (basic) path to use for example foo/bar to get 'value' from: { foo: { bar: 'value' } }
		 * @param object, the object to search in
		 * @return the value of the path or undefined when not found.
		 */
		function getValue(path, object) {
			path = $(path.split('/'));//.filter(function() { return this != ''; });
			for (var i = 0, l = path.length; i < l; i++) {
				object = object[path[i]];
				if (!object) {
					break;
				}
			}
			return object;
		}

		return this.each(function() {
			if ($(this).data('isAutocompleter')) {
				return;
			}

			var input = $(this).attr('autocomplete', 'off').off('keyup keydown'),
				settings = fetchSettings(input),
				list = null,
				items = null,
				selected = null,
				previousValue = input.val();

			// sets the value of the input and value input.
			function setValue(value, display) {
				input.val(display);
				if (settings.value) {
					settings.value.val(value);
				}
				if (typeof settings.onChange == 'function') {
					return settings.onChange(value, display, input);
				}
				return true;
			}

			// creates and fills the list when data is received
			function autocomplete(data) {
				if ($.isArray(data)) {
					if (data.length > 0) {
						if (!list) {
							// create a new list element
							list = $.apply(this, settings.list)
								.css({ width: input.outerWidth() })
								.insertAfter(input);
						} else {
							list.empty();
						}
						$(data).each(function() {
							var value = getValue(settings.valuePath, this),
								display = getValue(settings.displayPath, this);
							list.append(
								$.apply(this, settings.item)
									.data('value', value)
									.html(display)
									.on('click.Autocompleter', function(event) {
										if (setValue(value, display)) {
											list.hide();
										} else {
											window.clearTimeout(delayTimer);
											input.trigger('focus.Autocompleter');
										}
									})
							);
						});
						items = list.children();
						selected = items.first().addClass(settings.selectedClass);
						list.show();
					} else if (list) {
						list.empty();
						selected = null;
					}
				} else {
					throw "Result is not a valid Array";
				}
			}

			// adds a 'named' param to the given url
			// /foo/bar.json becomes /foo/bar/key:value.json
			function addNamedParam(url, key, value) {
				var parts = url.split('.'),
					i = parts.length > 1 ? parts.length - 2 : 0,
					param = key + ':' + value;
				parts[i] = parts[i].split('/');
				parts[i].push(param);
				parts[i] = parts[i].join('/');
				return parts.join('.');
			}

			/**
			 * Events
			 */
			input
			.on('Autocompleter.autocomplete', function(event, params) {
				var ajaxParams = $.extend({
						url: settings.action,
						type: settings.method,
						success: autocomplete,
						error: function(xhr, status, error) {
							if (typeof console != 'undefined' && typeof console.error == 'function') {
								console.error('Autocompleter:', status, error);
							}
						},
						complete: function(xhr, status) { }
					}, settings.ajax), value = input.val(), param;

				if (params) {
					if (typeof params == 'string') {
						value = params;
						input.val(value);
					}
				}

				// add the value parameter by replacing the token, adding the named parameter or setting the query parameter
				if (settings.token) { // replace a token in the url
					ajaxParams.url.replace(settings.token, value);
				} else if(settings.named) { // add a named parameter
					ajaxParams.url = addNamedParam(ajaxParams.url, settings.named, value);
				} else if (settings.param) { // set a query parameter
					param = {};
					param[settings.param] = value;
					ajaxParams.data = $.extend(ajaxParams.data || {}, param);
				}

				// all done, let parseSettings change things:
				ajaxParams = settings.parseSettings(ajaxParams);

				$.ajax(ajaxParams);
			})
			// keyup event, triggers keyboard navigation when the arrow keys are used or fires the ajax call
			.on('keydown.Autocompleter', function(event) {
				var value = input.val(), select = null;
				if (list) {
					switch (event.which) {
						case 38: // up
							select = selected.prev();
							if (!select.length) {
								select = null;
							}
						case 40: // down
							select = select || selected.next();
							if (select.length) {
								selected.removeClass(settings.selectedClass);
								selected = select.addClass(settings.selectedClass);
							}
							event.preventDefault();
							return false;
						break;
						case 13: // enter
							if (selected) {
								selected.trigger('click.Autocompleter');
							} else if (typeof settings.onInputValue == 'function') {
								settings.onInputValue(value);
							}
							event.preventDefault();
							return false;
						break;
					}
				} else if (typeof settings.onInputValue == 'function') {
					if (event.which == 13) {
						settings.onInputValue(value);
					}
				}
				// check for changes before fireing a new request:
				if (value != previousValue) {
					previousValue = value;
					window.clearTimeout(delayTimer);
					delayTimer = window.setTimeout(function() {
						input.trigger('Autocompleter.autocomplete');
					}, settings.delay);
				}
			})
			// hides the list:
			.on('blur.Autocompleter', function(event) {
				if (list) {
					window.clearTimeout(delayTimer);
					delayTimer = window.setTimeout(function() {
						list.hide();
					}, 100);
				}
				if (settings.allowEmpty && input.val() == '') {
					setValue('', '');
				}
			})
			// no duplicate instances:
			.data('isAutocompleter', true);
		});
	}
}(jQuery));